import Koa from 'koa';

async function ErrorHandler(
  ctx: Koa.ExtendableContext,
  next: Koa.Next
): Promise<void> {
  try {
    await next();
  } catch (e) {
    ctx.status = e.status || 500;
    ctx.body = { error: e.message };
    ctx.app.emit('error', e, ctx);
  }
}

export default ErrorHandler;
