import Router from 'koa-router';
import IBackend from '../../../service/interfaces/IBackend';
import getBytId from './getById';

export default (router: Router, backend: IBackend): void => {
  router.get('/api/v1/profile/:id', getBytId(backend));
};
