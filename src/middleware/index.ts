import Koa from 'koa';
import cors from '@koa/cors';
import helmet from 'koa-helmet';
import log4js from 'koa-log4';
import bodyParser from 'koa-bodyparser';
import passport from 'koa-passport';
import Passport from '../common/Passport';
import log from '../logger';
import ErrorHandler from '../common/ErrorHandler';

export default (app: Koa): void => {
  const logger = log('src/middleware/index.ts');
  app.use(log4js.koaLogger(logger, { level: 'auto' }));
  app.use(helmet());
  app.use(ErrorHandler);
  app.use(cors());
  app.use(bodyParser());
  Passport();
  app.use(passport.initialize());
};
