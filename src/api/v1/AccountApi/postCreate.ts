import Koa from 'koa';
import IBackend from '../../../service/interfaces/IBackend';

export default (
  backend: IBackend
): ((ctx: Koa.ExtendableContext) => Promise<void>) => {
  return async (ctx: Koa.ExtendableContext): Promise<void> => {
    const data = ctx.request.body;
    const account = backend.getAccount();
    const result = await account.createAccount(data);
    if (result.err) {
      throw result.err;
    }
    ctx.status = 201;
    ctx.body = { id: result.result };
  };
};
