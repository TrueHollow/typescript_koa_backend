import passport from 'koa-passport';
import PassportHttpBearer from 'passport-http-bearer';
import jose from 'node-jose';
import log from '../logger';
import KeyManager from './KeyManager';

const logger = log('src/common/Passport.ts');

const { Strategy } = PassportHttpBearer;

export default (): void => {
  passport.use(
    new Strategy(async (token, done) => {
      try {
        const keyJWK = await KeyManager.getKey();
        const joseJWE = await jose.JWE.createDecrypt(keyJWK).decrypt(token);
        const payloadString = joseJWE.plaintext.toString();
        const payload = JSON.parse(payloadString);

        if (payload) {
          done(null, payload);
        } else {
          done(null, false);
        }
      } catch (e) {
        logger.warn(e);
        done('JWE error', false);
      }
    })
  );
};
