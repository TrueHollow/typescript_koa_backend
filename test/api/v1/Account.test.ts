import chai, { expect } from 'chai';
import chaiHttp from 'chai-http';
import faker from 'faker';
import log from '../../../src/logger';

import server from '../../../src/app';

const logger = log('test/api/v1/Account.test.js');
chai.use(chaiHttp);

const userData = {
  email: faker.internet.email(),
  password: faker.internet.password(),
  userName: faker.internet.userName(),
};

let token: string;
// let uid;
describe('Test Accounts http methods', () => {
  before(async () => {
    logger.info(`Test before. Fake data: (${JSON.stringify(userData)})`);
  });
  after(async () => {
    logger.info('Test after');
    server.close();
  });
  it('test openapi html documentation endpoint', async () => {
    const res = await chai.request(server).get('/openapi');
    expect(res).to.have.status(200);
    // eslint-disable-next-line @typescript-eslint/no-unused-expressions
    expect(res).to.be.html;
  });
  it('test openapi json schema', async () => {
    const res = await chai.request(server).get('/openapi.json');
    expect(res).to.have.status(200);
    // eslint-disable-next-line @typescript-eslint/no-unused-expressions
    expect(res).to.be.json;
  });
  it('test get method (Entry point "/")', async () => {
    const res = await chai.request(server).get('/');
    expect(res).to.have.status(200);
    const { body } = res;
    expect(body)
      .to.be.an('object')
      .and.to.have.property('name');
  });
  it('test post method (Create Account)', async () => {
    const res = await chai
      .request(server)
      .post('/api/v1/account')
      .send(userData);
    expect(res).to.have.status(201);
    const { body } = res;
    expect(body)
      .to.be.an('object')
      .and.not.to.have.property('error');
    expect(body.id).to.be.an('number');
  });
  it('test post method (Invalid login)', async () => {
    const res = await chai
      .request(server)
      .post('/api/v1/account/login')
      .send({
        email: 'invalid@email.com',
        password: 'invalidPassword',
      });
    expect(res).to.have.status(401);
  });
  it('test post method (Login)', async () => {
    const res = await chai
      .request(server)
      .post('/api/v1/account/login')
      .send({
        email: userData.email,
        password: userData.password,
      });
    expect(res).to.have.status(200);
    const { body } = res;
    expect(body)
      .to.be.an('object')
      .and.to.have.property('token')
      .and.not.to.have.property('err');
    expect(body.token).to.be.a('string');
    token = body.token;
  });
  it('test put method (Update account)', async () => {
    userData.userName = faker.internet.userName();
    const res = await chai
      .request(server)
      .put('/api/v1/account')
      .set('Authorization', `Bearer ${token}`)
      .send(userData);
    expect(res).to.have.status(200);
    const { body } = res;
    expect(body)
      .to.be.an('object')
      .and.to.have.property('success')
      .and.not.to.have.property('err');
  });
  // it('test post method (upload image to Account profile)', async () => {
  //   const imagePath = path.resolve(__dirname, 'res', 'apple.png');
  //   const res = await chai
  //     .request(server)
  //     .post('/api/v1/upload')
  //     .attach('image', fs.readFileSync(imagePath), 'apple.png')
  //     .set('Authorization', `Bearer ${token}`);
  //   expect(res).to.have.status(200);
  //   const { body } = res;
  //   expect(body)
  //     .to.be.an('object')
  //     .and.to.have.a.property('result')
  //     .and.not.to.have.a.property('err');
  // });
  // it('test get method (my Account)', async () => {
  //   const res = await chai
  //     .request(server)
  //     .get('/api/v1/account')
  //     .set('Authorization', `Bearer ${token}`);
  //   expect(res).to.have.status(200);
  //   const { body } = res;
  //   const copy = {
  //     userName: userData.userName,
  //     email: userData.email,
  //   };
  //   expect(body)
  //     .to.be.an('object')
  //     .and.to.include(copy);
  // });
  // it('test get method (activate my Account)', async () => {
  //   const url = `/api/v1/account/activate/${uid}`;
  //   const res = await chai.request(server).get(url);
  //   expect(res).to.have.status(200);
  //   const { body } = res;
  //   expect(body)
  //     .to.be.an('object')
  //     .and.to.have.a.property('result')
  //     .and.not.to.have.a.property('err');
  // });
});
