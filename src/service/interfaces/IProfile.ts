import IResult from './IResult';

export default interface Profile {
  getById(id: number): Promise<IResult>;
}
