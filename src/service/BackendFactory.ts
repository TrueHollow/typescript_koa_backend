import IBackend from './interfaces/IBackend';
import Backend from './classes/Backend';

class BackendFactory {
  constructor(backend: IBackend = new Backend()) {
    this.backend = backend;
  }

  getBackend(): IBackend {
    return this.backend;
  }

  private readonly backend: IBackend;
}

export default BackendFactory;
