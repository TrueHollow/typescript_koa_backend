import * as knex from 'knex';
import dotEnvProxy from '../common/dotEnv_proxy';

dotEnvProxy();

const connection = {
  host: process.env.DATABASE_HOST,
  port: process.env.DATABASE_PORT,
  database: process.env.DATABASE_NAME,
  user: process.env.DATABASE_USER,
  password: process.env.DATABASE_PASSWORD,
} as knex.MariaSqlConnectionConfig;

export const development = {
  client: 'mysql',
  connection: process.env.DATABASE_URL || connection,
  pool: {
    min: 2,
    max: 10,
  },
  debug: true,
  migrations: {
    directory: './migrations',
    tableName: 'migrations',
  },
  seeds: {
    directory: './seeds',
  },
} as knex.Config;

export const production = {
  client: 'mysql',
  connection: process.env.DATABASE_URL || connection,
  pool: {
    min: 2,
    max: 10,
  },
  migrations: {
    directory: './migrations',
    tableName: 'migrations',
  },
  seeds: {
    directory: './seeds',
  },
} as knex.Config;

export const getConfig = (): knex.Config => {
  switch (process.env.NODE_ENV) {
    case 'production':
      return production;
    case 'development':
    default:
      return development;
  }
};
