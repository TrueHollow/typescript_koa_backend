import Koa from 'koa';
import createHttpError from 'http-errors';
import IBackend from '../../../service/interfaces/IBackend';

export default (
  backend: IBackend
): ((ctx: Koa.ParameterizedContext) => Promise<void>) => {
  return async (ctx: Koa.ParameterizedContext): Promise<void> => {
    const data = ctx.request.body;
    const { account: acc } = ctx.state.user;
    const account = backend.getAccount();
    const result = await account.updateAccount(acc.id, data);
    if (result.err) {
      throw createHttpError(400, result.err);
    }
    ctx.body = {
      success: true,
    };
  };
};
