import jose, { JWK } from 'node-jose';
import config from '../config';

import Key = JWK.Key;

const { jwt } = config;

let key: Key | null = null;

export default class KeyManager {
  static async getKey(): Promise<Key> {
    if (key === null) {
      key = await jose.JWK.asKey(jwt.key);
    }
    return key;
  }
}
