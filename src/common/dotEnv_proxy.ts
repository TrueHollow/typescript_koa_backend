import * as path from 'path';
import * as dotenv from 'dotenv';
import log from '../logger';

const envFilePath = path.resolve(__dirname, '..', '..', '.env');
const logger = log('src/common/dotEnv_proxy.ts');

let isInit = false;

interface IoError extends Error {
  code?: string;
}

export default (): void => {
  if (isInit) {
    return;
  }
  isInit = true;
  const dotParsingResult = dotenv.config({ path: envFilePath });
  if (dotParsingResult.error) {
    const error = dotParsingResult.error as IoError;
    if (error.code === 'ENOENT') {
      logger.debug('Environment file do not exist. Skipping.');
    } else {
      logger.warn('Unexpected dotenv: (%s)', JSON.stringify(error));
    }
  }
};
