import Koa from 'koa';
import Router from 'koa-router';
import { oas } from 'koa-oas3';
import path from 'path';
import log from './logger';
import middleware from './middleware';
import envProxy from './common/dotEnv_proxy';
import apiv1 from './api/v1/index';
import BackendFactory from './service/BackendFactory';
import config from './config';

envProxy();
const logger = log('src/app.ts');

logger.info('App started.');

const app = new Koa();
app.on('error', (err /* , ctx */) => {
  logger.error(
    `Error: (${err.status || 500}), message: ${err.message}, name: ${err.name}`
  );
});

// trust proxy
app.proxy = true;

middleware(app);
const openApiFullPath = path.resolve(__dirname, '..', 'openapi.yaml');
app.use(
  oas({
    file: openApiFullPath,
    enableUi: true,
    endpoint: '/openapi.json',
    uiEndpoint: '/openapi',
    validateResponse: true,
  })
);

const router = new Router();
const factory = new BackendFactory();
apiv1(router, factory.getBackend());

app.use(router.routes()).use(router.allowedMethods());

const port: number = process.env.PORT
  ? Number.parseInt(process.env.PORT, 10)
  : config.app.PORT;

app.on('error', (err, ctx) => {
  logger.error('server error', err, ctx);
});
const server = app.listen(port, () => logger.info(`Listening port ${port}`));

export default server;
