import Koa from 'koa';
import IBackend from '../../../service/interfaces/IBackend';

export default (
  backend: IBackend
): ((ctx: Koa.ExtendableContext) => Promise<void>) => {
  return async (ctx: Koa.ExtendableContext): Promise<void> => {
    const { id } = ctx.request.query;
    const profile = backend.getProfile();
    const operationResult = await profile.getById(id);
    if (operationResult.err) {
      throw operationResult.err;
    }
    const { result } = operationResult;
    if (!result) {
      ctx.status = 404;
    }
    ctx.body = { result };
  };
};
