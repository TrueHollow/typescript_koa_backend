exports.up = async knex => {
  return knex.schema.createTable('accounts', table => {
    table
      .increments('id')
      .unsigned()
      .notNullable()
      .primary();
    table.string('email', 150).unique();
    table.string('username', 50);
    table.string('password', 150);
    table.timestamp('created_at').defaultTo(knex.fn.now());
    table.timestamp('updated_at').defaultTo(knex.fn.now());
  });
};

exports.down = async knex => {
  return knex.schema.dropTable('accounts');
};
