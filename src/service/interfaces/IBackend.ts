import IAccount from './IAccount';
import IProfile from './IProfile';

export default interface Backend {
  getName(): string;
  getVersion(): string;
  getAccount(): IAccount;
  getProfile(): IProfile;
}
