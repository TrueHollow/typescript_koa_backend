import IBackend from '../interfaces/IBackend';
import IAccount from '../interfaces/IAccount';
import Account from './Account';
import IProfile from '../interfaces/IProfile';
import Profile from './Profile';

export default class Backend implements IBackend {
  private readonly profile: IProfile;

  private readonly account: IAccount;

  private readonly version: string;

  constructor() {
    this.account = new Account();
    this.profile = new Profile();
    this.version = '1.0';
  }

  getName(): string {
    return `Backend version ${this.getVersion()}`;
  }

  getVersion(): string {
    return this.version;
  }

  getAccount(): IAccount {
    return this.account;
  }

  getProfile(): IProfile {
    return this.profile;
  }
}
