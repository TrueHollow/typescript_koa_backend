import Koa from 'koa';
import jose from 'node-jose';
import createHttpError from 'http-errors';
import KeyManager from '../../../common/KeyManager';
import IBackend from '../../../service/interfaces/IBackend';

export default (
  backend: IBackend
): ((ctx: Koa.ExtendableContext) => Promise<void>) => {
  return async (ctx: Koa.ExtendableContext): Promise<void> => {
    const data = ctx.request.body;
    const account = backend.getAccount();
    const result = await account.loginIntoAccount(data);
    if (result.err) {
      throw createHttpError(401, result.err);
    }
    const tokenData = {
      account: {
        id: result.result,
      },
      timestamp: new Date(),
    };
    const tokenString = JSON.stringify(tokenData);
    const key = await KeyManager.getKey();
    const token = await jose.JWE.createEncrypt({ format: 'compact' }, key)
      .update(tokenString)
      .final();
    ctx.body = {
      token,
    };
  };
};
