import Router from 'koa-router';
import passport from 'koa-passport';
import postCreate from './postCreate';
import putUpdate from './putUpdate';
import postLogin from './postLogin';
import IBackend from '../../../service/interfaces/IBackend';
import config from '../../../config';

export default (router: Router, backend: IBackend): void => {
  router.post('/api/v1/account', postCreate(backend));
  router.put(
    '/api/v1/account',
    passport.authenticate('bearer', config.jwt.session),
    putUpdate(backend)
  );
  router.post('/api/v1/account/login', postLogin(backend));
};
