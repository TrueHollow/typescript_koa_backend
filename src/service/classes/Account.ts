import bcrypt from 'bcrypt';
import log4js from 'log4js';
import IResult from '../interfaces/IResult';
import IAccount from '../interfaces/IAccount';
import AccountModel from '../models/AccountModel';
import knex from '../../db';
import log from '../../logger';

/**
 * bcrypt salt round
 * @type {number}
 */
const BCRYPT_ROUNDS = 6;

/**
 * Account class for current backend
 */
export default class Account implements IAccount {
  /**
   * Internal logger (for avoiding issues related to eslint "this" checking
   * inside of methods)
   */
  private readonly logger: log4js.Logger;

  /**
   * Constructor. Will create internal logger.
   */
  constructor() {
    this.logger = log('src/service/classes/Account.ts');
  }

  /**
   * Create Account method.
   * @param {AccountModel} accountData
   * @returns {Promise<Result>}
   */
  async createAccount(accountData: AccountModel): Promise<IResult> {
    this.logger.debug('createAccount');
    const result: AccountModel = accountData;
    result.password = await bcrypt.hash(accountData.password, BCRYPT_ROUNDS);
    const trx = await knex.transaction();
    const [id] = await trx('accounts').insert(result);
    await trx('profiles').insert({ account_id: id });
    await trx.commit();
    return { result: id };
  }

  async updateAccount(id: number, accountData: AccountModel): Promise<IResult> {
    this.logger.debug('updateAccount');
    const result = accountData;
    if (result.password) {
      result.password = await bcrypt.hash(result.password, BCRYPT_ROUNDS);
    }
    await knex('accounts')
      .update(result)
      .where('id', id);

    return { result: true };
  }

  async loginIntoAccount(accountData: AccountModel): Promise<IResult> {
    this.logger.debug('loginIntoAccount');
    const [account] = await knex('accounts')
      .select('id', 'password')
      .where('email', accountData.email);
    if (!account) {
      return {
        err: 'No user with email found',
      };
    }
    const passportIsValid = await bcrypt.compare(
      accountData.password,
      account.password
    );
    if (!passportIsValid) {
      return {
        err: 'Password is invalid',
      };
    }
    return {
      result: account.id,
    };
  }

  async getAccountByEmail(email: string): Promise<IResult> {
    this.logger.debug('getAccountByEmail');
    const [dataRow] = await knex('accounts')
      .select('id')
      .where('email', email)
      .limit(1);
    if (dataRow) {
      return { result: dataRow };
    }
    return { err: '' };
  }
}
