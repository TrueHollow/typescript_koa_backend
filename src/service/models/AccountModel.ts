class AccountModel {
  id: number | null | undefined;

  email: string | null | undefined;

  username: string | null | undefined;

  password: string | null | undefined;

  created_at: string | null | undefined;

  updated_at: string | null | undefined;
}

export default AccountModel;
