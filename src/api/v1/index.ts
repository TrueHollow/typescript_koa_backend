import Router from 'koa-router';
import IBackend from '../../service/interfaces/IBackend';
import AccountApi from './AccountApi';
import ProfileApi from './ProfileApi';

export default (router: Router, backend: IBackend): void => {
  router.get('/', ctx => {
    ctx.body = {
      name: backend.getName(),
      version: backend.getVersion(),
    };
  });
  AccountApi(router, backend);
  ProfileApi(router, backend);
};
