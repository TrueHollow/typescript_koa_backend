export default {
  log4js: {
    appenders: {
      console: {
        type: 'stdout',
      },
    },
    categories: {
      default: {
        appenders: ['console'],
        level: 'debug',
      },
    },
  },
  app: {
    PORT: 3000,
  },
  jwt: {
    key: {
      kty: 'oct',
      kid: 'X1Xt4rZ63O6pVoGxdQd-b5XaIj5vHzsxBxVW327ctOA',
      alg: 'A256GCM',
      k: '64SNqNPRHLVz1DPTKwHqea8dX6uRnsKt7Nyg5fuEssM',
    },
    session: {
      session: false,
    },
  },
};
