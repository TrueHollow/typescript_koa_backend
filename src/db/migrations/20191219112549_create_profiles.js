exports.up = async knex => {
  return knex.schema.createTable('profiles', table => {
    table
      .increments('id')
      .unsigned()
      .notNullable()
      .primary();
    table
      .integer('account_id')
      .unsigned()
      .unique()
      .references('accounts.id')
      .onDelete('CASCADE')
      .onUpdate('CASCADE');
    table.string('avatar', 50);
    table.string('address', 150);
    table.string('phone', 10);
    table.timestamp('created_at').defaultTo(knex.fn.now());
    table.timestamp('updated_at').defaultTo(knex.fn.now());
  });
};

exports.down = async knex => {
  return knex.schema.dropTable('profiles');
};
