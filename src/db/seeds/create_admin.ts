import AccountModel from '../../service/models/AccountModel';
import BackendFactory from '../../service/BackendFactory';

const adminUsername = 'Admin';
const adminEmail = 'admin@example.com';
const adminPassword = 'secret';

// eslint-disable-next-line import/prefer-default-export
export async function seed(): Promise<void> {
  const factory = new BackendFactory();
  const backend = factory.getBackend();
  const account = backend.getAccount();
  const acc = await account.getAccountByEmail(adminEmail);
  if (acc) {
    return;
  }
  const data = {
    email: adminEmail,
    password: adminPassword,
    username: adminUsername,
  } as AccountModel;
  await account.createAccount(data);
}
