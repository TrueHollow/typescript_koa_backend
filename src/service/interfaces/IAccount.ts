import IResult from './IResult';
import AccountModel from '../models/AccountModel';

/**
 * Interface declaring Account methods
 */
export default interface Account {
  createAccount(accountData: AccountModel): Promise<IResult>;
  loginIntoAccount(accountData: AccountModel): Promise<IResult>;
  updateAccount(id: number, accountData: AccountModel): Promise<IResult>;
  getAccountByEmail(email: string): Promise<IResult>;
}
