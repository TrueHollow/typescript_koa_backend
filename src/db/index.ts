import Knex from 'knex';
import * as knexConfig from './knexfile';

const knex = Knex(knexConfig.getConfig());

export default knex;
