import log4js from 'log4js';
import knex from '../../db';
import IProfile from '../interfaces/IProfile';
import log from '../../logger';
import IResult from '../interfaces/IResult';

export default class Profile implements IProfile {
  /**
   * Internal logger (for avoiding issues related to eslint "this" checking
   * inside of methods)
   */
  private readonly logger: log4js.Logger;

  /**
   * Constructor. Will create internal logger.
   */
  constructor() {
    this.logger = log('src/service/classes/Account.ts');
  }

  async getById(id: number): Promise<IResult> {
    this.logger.debug(`getById: ${id}`);
    const [profile] = await knex('profiles')
      .select('!id')
      .where('id', id);
    return { result: profile };
  }
}
